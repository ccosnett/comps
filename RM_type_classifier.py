import as as pd
import numpy as np
import math
import re
from pandarallel import pandarallel
pandarallel.initialize(nb_workers=25, progress_bar=True)
from tqdm.auto import tqdm
tqdm.pandas(desc="attach column")
import time
start_time = time.time()



#RM_file = "./groot.csv"
#RM_file = "/home/ubuntu/Dropbox/09_RM_data/RM_BUY_6_28_2021.csv"
# assuming that /remote_valdev_4_upload/08_upload_RM.sh has been run first and so has 18_preprocess_RM.sql
RM_file = "/home/ubuntu/Dropbox/07_liquidity/91_Liqquid/upload_and_preprocess/remote_valdev_2_upload/data/buy.csv"


# here we just setup a dataframe called 'out'. The rest of this program attaches columns to this dataframe.
print('data loadeding')
input_df=pd.read_csv(RM_file)
print('len = '+str(len(input_df)))
print('data loaded')
out = pd.DataFrame()
out["id"] = input_df["id"]
out["type"] = input_df["type"]
out["address"] = input_df["address"]
out["keyFeatures"] = input_df["keyFeatures"]
out["description"] = input_df["description"]

# cleaning up the format of the price column
print('cleaning up the format of the price column')
out["price"] = input_df["price"]
out['price'] = out['price'].str.replace(',','')
out['price'] = out['price'].str.replace('£','')
print('price cleaned up')

# cleaning up the format of the price column


def attach(func):
    '''this function takes as its argument a function which it uses to create and attach a column to the right of the dataframe called out'''
    out[func] = out.parallel_apply(    eval(func),    axis=1)


def search(regex, string):
    '''this function returns a boolean after searching a string for a regular expression'''
    return bool(re.search(regex, str(string).lower()))

################# Boolean functions ########################################

def single_true(iterable):
    '''single true takes a list of booleans called iterable as its argument and returns True if a single boolean is True'''
    i = iter(iterable)
    return any(i) and not any(i)

############################## FLAT FUNCTIONS #####################################


def fAJ_sub_filter1_house_flat_(row):
    black_lis = [
        r'detached bungalow',
        r'semi-detached bungalow',
        r'penthouse'
    ]
    bools1 = [search(i, row["type"]) for i in black_lis]
    lis=[
        r'apartment',
        r'bungalow',
        r'character property',
        r'cottage',
        r'detached',
        r'duplex',
        r'end of terrace',
        r'flat',
        r'ground flat',
        r'ground maisonette',
        r'house',
        r'maisonette',
        r'not specified',
        r'retirement property',
        r'semi-detached',
        r'sheltered housing',
        r'terraced',
        r'terraced bungalow',
        r'town house',
        r'village house'
    ]
    bools2 = [search(i, row["type"]) for i in lis]
    if any(bools1):
        return False
    elif any(bools2):
        return True
    else:
        return False


def fAK_sub_filter2_flat_(row):
    lis=[
        r'apartment',
        r'character property',
        r'duplex',
        r'flat',
        r'ground flat',
        r'ground maisonette',
        r'maisonette',
        r'not specified',
        r'retirement property',
        r'sheltered housing'
    ]
    bools = [search(i, row["type"]) for i in lis]
    if any(bools):
        return True
    else:
        return False

#f01_Suite_address = lambda row : search(r"suite", row["address"])    
def f01_Suite_address(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"suite", row["address"]):
            return True
        else:
            return False
    else:
        return False
    
#f02_Flat_address = lambda row : search(r"flat", row["address"])
def f02_Flat_address(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"flat", row["address"]):
            return True
        else:
            return False
    else:
        return False

#f03_Apartment_address = lambda row : search(r"apartment", row["address"])
def f03_Apartment_address(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"apartment", row["address"]):
            return True
        else:
            return False
    else:
        return False

#f04_Flat_type = lambda row : search(r"flat", row["type"])
def f04_Flat_type(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"flat", row["type"]):
            return True
        else:
            return False
    else:
        return False

#f05_Apartment_type = lambda row : search(r"apartment", row["type"])
def f05_Apartment_type(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"apartment", row["type"]):
            return True
        else:
            return False
    else:
        return False

#f06_Apartment_keyFeatures = lambda row : search(r"apartment", row["keyFeatures"])
def f06_Apartment_keyFeatures(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"apartment", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False
    
#f07_Flat_keyFeatures = lambda row : search(r"flat", row["keyFeatures"])    
def f07_Flat_keyFeatures(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"flat", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False   


#f08_Apts_keyFeatures = lambda row : search(r"apts", row["keyFeatures"])
def f08_Apts_keyFeatures(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"apts", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False   


#f09_Lift_keyFeatures = lambda row : search(r"lift to all floors", row["keyFeatures"])
def f09_Lift_keyFeatures(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"lift to all floors", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False   
    
#f10_Apartment_description = lambda row : search(r"apartment", row["description"])
def f10_Apartment_description(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"apartment", row["description"]):
            return True
        else:
            return False
    else:
        return False   

#f11_Flat_description = lambda row : search(r"flat", row["description"])
def f11_Flat_description(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"flat", row["description"]):
            return True
        else:
            return False
    else:
        return False   

#f12_Lift_description = lambda row : search(r"lift to all floors", row["description"])
def f12_Lift_description(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"lift to all floors", row["description"]):
            return True
        else:
            return False
    else:
        return False   

def f13_Flat(row):
    return any([
        row['f01_Suite_address'],
        row['f02_Flat_address'],
        row['f03_Apartment_address'],
        row['f04_Flat_type'],
        row['f05_Apartment_type'],
        row['f06_Apartment_keyFeatures'],
        row['f07_Flat_keyFeatures'],
        row['f08_Apts_keyFeatures'],
        row['f09_Lift_keyFeatures'],
        row['f10_Apartment_description'],
        row['f11_Flat_description'],
        row['f12_Lift_description']
               ])

############################## MAISONETTE FUNCTIONS #####################################

#f14_Maisonette_type = lambda row : search(r"maisonette", row["type"])
def f14_Maisonette_type(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"maisonette", row["type"]):
            return True
        else:
            return False
    else:
        return False 

#f15_Maisonette_keyFeatures = lambda row : search(r"maisonette", str(row["keyFeatures"]))
def f15_Maisonette_keyFeatures(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"maisonette", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False 

#f16_Maisonette_description = lambda row : search(r"maisonette", row["description"])
def f16_Maisonette_description(row):
    if fAK_sub_filter2_flat_(row):
        if search(r"maisonette", row["description"]):
            return True
        else:
            return False
    else:
        return False 

def f17_Maisonette(row):
    return any([
        row['f14_Maisonette_type'],
        row['f15_Maisonette_keyFeatures'],
        row['f16_Maisonette_description']
               ])


############################## BUNGALOW FUNCTIONS #####################################

#f18_Bungalow_type = lambda row : search(r"bungalow", row["type"])
def f18_Bungalow_type(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"bungalow", row["type"]):
            return True
        else:
            return False
    else:
        return False  

#f19_Bungalow_keyFeatures = lambda row : search(r"bungalow", row["keyFeatures"])
def f19_Bungalow_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"bungalow", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False 

#f20_Bungalow__description = lambda row : search(r"bungalow ", row["description"])
def f20_Bungalow__description(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"bungalow ", row["description"]):
            return True
        else:
            return False
    else:
        return False 

def f21_Bungalow(row):
    return any([
        row['f18_Bungalow_type'],
        row['f19_Bungalow_keyFeatures'],
        row['f20_Bungalow__description']
               ])

############################## SEMI-DETACHED FUNCTIONS #####################################

#f22_Semi_Detached_type = lambda row : search(r"semi(-|\s)detached", row["type"])
def f22_Semi_Detached_type(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"semi(-|\s)detached", row["type"]):
            return True
        else:
            return False
    else:
        return False 

#f23_Semi_Detached_keyFeatures = lambda row : search(r"semi(-|\s)detached", row["keyFeatures"])
def f23_Semi_Detached_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"semi(-|\s)detached", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False 

#f24_Semi_Detached__description = lambda row : search(r"semi(-|\s)detached", row["description"])
def f24_Semi_Detached__description(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"semi(-|\s)detached", row["description"]):
            return True
        else:
            return False
    else:
        return False 

def f25_Semi_detached(row):
    return any([
        row['f22_Semi_Detached_type'],
        row['f23_Semi_Detached_keyFeatures'],
        row['f24_Semi_Detached__description']
               ])


############################## DETACHED FUNCTIONS #####################################

#f26_Detached_type1 = lambda row : search(r"detached", row["type"])
def f26_Detached_type1(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"detached", row["type"]) and len(row["type"])==8:
            return True
        else:
            return False
    else:
        return False 

#f27_Detached_type2 = lambda row : search(r"detached", row["type"])
def f27_Detached_type2(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r"link detached house", row["type"]):
            return True
        else:
            return False
    else:
        return False 

#f28_Detached_keyFeatures = lambda row : search(r" detached", row["keyFeatures"])
def f28_Detached_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r" detached", row["keyFeatures"]):
            return True
        else:
            return False
    else:
        return False 

#f29_Detached_description = lambda row : search(r" detached", row["description"])
def f29_Detached_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        if search(r" detached", row["description"]):
            return True
        else:
            return False
    else:
        return False 

def f30_Detached(row):
    return any([
        row['f26_Detached_type1'],
        row['f27_Detached_type2'],
        row['f28_Detached_keyFeatures'],
        row['f29_Detached_description']
     ])


############################## END-OF-TERRACE FUNCTIONS #####################################

#f31_End_Of_Terrace_type = lambda row : (search(r"end(-|\s)of(-|\s)terrace", row["type"]) or search(r"end(-|\s)of(-|\s)terrace",row["type"]))
def f31_End_Of_Terrace_type(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"end(-|\s)of(-|\s)terrace", row["type"])
        b = search(r"end(-|\s)terrace", row["type"])
        if a or b:
            return True
        else:
            return False
    else:
        return False

#f32_End_Of_Terrace_keyFeatures = lambda row : search(r"end(-|\s)of(-|\s)terrace", row["keyFeatures"])
def f32_End_Of_Terrace_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"end(-|\s)of(-|\s)terrace", row["keyFeatures"])
        b = search(r"end(-|\s)terrace", row["keyFeatures"])
        if a or b:
            return True
        else:
            return False
    else:
        return False

#f33_End_Of_Terrace_description = lambda row : search(r"end(-|\s)of(-|\s)terrace", row["description"])
def f33_End_Of_Terrace_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"end(-|\s)of(-|\s)terrace", row["description"])
        b = search(r"end(-|\s)terrace", row["description"])
        if a or b:
            return True
        else:
            return False
    else:
        return False



def f34_End_Of_Terrace(row):
    return any([
        row['f31_End_Of_Terrace_type'],
        row['f32_End_Of_Terrace_keyFeatures'],
        row['f33_End_Of_Terrace_description']
    ])


############################## MID-TERRACE FUNCTIONS #####################################

#f35_Mid_Terrace_keyFeatures = lambda row : search(r"mid(-|\s)terrace", row["keyFeatures"])
def f35_Mid_Terrace_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"mid(-|\s)terrace", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f36_Mid_Terrace_description = lambda row : search(r"mid(-|\s)terrace", row["description"])
def f36_Mid_Terrace_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"mid(-|\s)terrace", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f37_Mid_Terrace(row):
    return any([
        row['f35_Mid_Terrace_keyFeatures'],
        row['f36_Mid_Terrace_description']
     ])

############################## TERRACED FUNCTIONS #####################################

#f38_Terraced_type = lambda row : search(r"terraced", row["type"])
def f38_Terraced_type(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"terraced", row["type"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f39_Terraced_keyFeatures = lambda row : search(r"terraced", row["keyFeatures"])
def f39_Terraced_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"terraced", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f40_Terraced_description = lambda row : search(r"terraced", row["description"])
def f40_Terraced_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"terraced", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f41_Terraced(row):
    return any([
        row['f38_Terraced_type'],
        row['f39_Terraced_keyFeatures'],
        row['f40_Terraced_description']
     ])



############################## COTTAGE FUNCTIONS #####################################

#f42_Cottage_type = lambda row : search(r"cottage", row["type"])
def f42_Cottage_type(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"cottage", row["type"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f43_Cottage_keyFeatures = lambda row : search(r"cottage", row["keyFeatures"])
def f43_Cottage_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"cottage", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f44_Cottage_description = lambda row : search(r"cottage", row["description"])
def f44_Cottage_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"cottage", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f45_Cottage(row):
    return any([
        row['f42_Cottage_type'],
        row['f43_Cottage_keyFeatures'],
        row['f44_Cottage_description']
     ])


############################## PARK HOME FUNCTIONS #####################################

#f46_Park_Home_keyFeatures = lambda row : search(r"park home", row["keyFeatures"])
def f46_Park_Home_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"park home", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f47_Park_Home_description = lambda row : search(r"park home", row["description"])
def f47_Park_Home_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"park home", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f48_Park_Home(row):
    return any([
        row['f46_Park_Home_keyFeatures'],
        row['f47_Park_Home_description']
     ])

############################## LEASEHOLD FUNCTIONS #####################################

#f49_Leasehold_keyFeatures = lambda row : search(r"leasehold", row["keyFeatures"])
def f49_Leasehold_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"leasehold", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False


#f50_Leasehold_description = lambda row : search(r"leasehold", row["description"])
def f50_Leasehold_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"leasehold", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f52_Leasehold(row):
    return any([
        row['f49_Leasehold_keyFeatures'],
        row['f50_Leasehold_description']
    ])

############################## FREEHOLD FUNCTIONS #####################################

#f53_Freehold_keyFeatures = lambda row : search(r"freehold", row["keyFeatures"])
def f53_Freehold_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"freehold", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f54_Freehold_description = lambda row : search(r"freehold", row["description"])
def f54_Freehold_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"freehold", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f55_Freehold(row):
    return any([
        row['f53_Freehold_keyFeatures'],
        row['f54_Freehold_description']
    ])


############################## FAMILY PROPERTY FUNCTIONS #####################################

#f56_Family_Property_keyFeatures = lambda row : search(r"family property", row["keyFeatures"])
def f56_Family_Property_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"family property", row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f57_Family_Property_description = lambda row : search(r"family property", row["description"])
def f57_Family_Property_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(r"family property", row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False

def f58_Family_Property(row):
    return any([
        row['f56_Family_Property_keyFeatures'],
        row['f57_Family_Property_description']
    ])


############################## RETIREMENT PROPERTY FUNCTIONS #####################################
retirementRegex = r"over(\s|-)55(s|'s)"
#f59_Retirement_Property_type = lambda row : search(r"retirement property", row["type"])
def f59_Retirement_Property_type(row):
    return search(r"retirement property", row["type"])

#f60_Retirement_Property_o55_keyFeatures = lambda row : search(retirementRegex, row["keyFeatures"])
def f60_Retirement_Property_o55_keyFeatures(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(retirementRegex, row["keyFeatures"])
        if a:
            return True
        else:
            return False
    else:
        return False

#f61_Retirement_Property_keyFeatures = lambda row : search(r"retirement property", row["keyFeatures"])
def f61_Retirement_Property_keyFeatures(row):
    return search(r"retirement property", row["keyFeatures"])

#f62_Retirement_Property_o55_description = lambda row : search(retirementRegex, row["description"])
def f62_Retirement_Property_o55_description(row):
    if fAJ_sub_filter1_house_flat_(row):
        a = search(retirementRegex, row["description"])
        if a:
            return True
        else:
            return False
    else:
        return False


#f63_Retirement_Property_description = lambda row : search(r"retirement property", row["description"])
def f63_Retirement_Property_description(row):
    return search(r"retirement property", row["description"])


def f64_Retirement_Property(row):
    return any([
        row['f59_Retirement_Property_type'],
        row['f60_Retirement_Property_o55_keyFeatures'],
        row['f61_Retirement_Property_keyFeatures'],
        row['f62_Retirement_Property_o55_description'],
        row['f63_Retirement_Property_description']

     ])


############################## COACH HOUSE FUNCTIONS #####################################

#f64_z_Coach_House_type = lambda row : search(r"coach house", row["type"])

def f64_z_Coach_House_type(row):
    return search(r"coach house", row["type"])

#f57_Family_Property_description = lambda row : search(r"Family property", row["description"])
#def f58_Family_Property(row):
#    return any([
#        row['f56_Family_Property_keyFeatures'],
#        row['f57_Family_Property_description']
#    ])


############################## TYPE 4 Codes #####################################

def f65_FS_Flat_Semi_Detached(row):  #1
    return row['f13_Flat'] and row['f25_Semi_detached'] and not row['f17_Maisonette']


def f66_FE_Flat_End_Terrace(row):   #2
    return row['f13_Flat'] and row['f34_End_Of_Terrace'] and not row['f17_Maisonette']


def f67_FM_Flat_Mid_Terrace(row): #3
    return row['f13_Flat'] and row['f37_Mid_Terrace'] and not row['f17_Maisonette']
 
    
def f68_FD_Flat_Detached(row): #4
    return row['f13_Flat'] and row['f30_Detached'] and not row['f17_Maisonette'] and not row['f65_FS_Flat_Semi_Detached']


def f69_F_Flat(row): #5    
    return row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f65_FS_Flat_Semi_Detached']\
and not row['f66_FE_Flat_End_Terrace']\
and not row['f67_FM_Flat_Mid_Terrace']\
and not row['f68_FD_Flat_Detached']


def f70_MS_Maisonette_Semi_Detached(row): #6   
    return row['f17_Maisonette'] and row['f25_Semi_detached']


def f71_ME_Maisonette_End_Terrace(row): #7  
    return row['f17_Maisonette'] and row['f34_End_Of_Terrace']


def f72_MM_Maisonette_Mid_Terrace(row): #8 
    return row['f17_Maisonette'] and row['f37_Mid_Terrace']


def f73_MD_Maisonette_Detached(row): #9  
    return row['f17_Maisonette'] and row['f30_Detached'] and not row['f70_MS_Maisonette_Semi_Detached']

def f74_M_Maisonette(row): #10  
    return row['f17_Maisonette']\
and not row['f70_MS_Maisonette_Semi_Detached']\
and not row['f71_ME_Maisonette_End_Terrace']\
and not row['f72_MM_Maisonette_Mid_Terrace']\
and not row['f73_MD_Maisonette_Detached']


def f75_BS_Bungalow_Semi_Detached(row): #11
    return row['f21_Bungalow'] and row['f25_Semi_detached']

def f76_BE_Bungalow_End_Terrace(row): #12
    return row['f21_Bungalow'] and row['f34_End_Of_Terrace']

#def f77_BM_Bungalow_Mid_Terrace(row): #13
 #   return row['f21_Bungalow'] and row['f37_Mid_Terrace']

def f77_BM_Bungalow_Mid_Terrace(row): #13
    return row['f21_Bungalow'] and (row['f37_Mid_Terrace'] or row['f41_Terraced']) and not row['f34_End_Of_Terrace']

def f78_BD_Bungalow_Detached(row): #14
    return row['f21_Bungalow'] and row['f30_Detached'] and not row['f75_BS_Bungalow_Semi_Detached']

def f79_B_Bungalow(row): #15
    return row['f21_Bungalow']\
and not row['f75_BS_Bungalow_Semi_Detached']\
and not row['f76_BE_Bungalow_End_Terrace']\
and not row['f77_BM_Bungalow_Mid_Terrace']\
and not row['f78_BD_Bungalow_Detached']

#def f80_TE_End_Of_Terrace(row): #16
#    return row['f34_End_Of_Terrace']\
#and not row['f13_Flat']\
#and not row['f17_Maisonette']\
#and not row['f21_Bungalow']\
#and not row['f26_Detached_type1']\
#and not row['f27_Detached_type2']\
#and not row['f25_Semi_detached']


def f80_TE_End_Of_Terrace(row): #16
    return row['f34_End_Of_Terrace']\
and not row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f21_Bungalow']


def f81_TM_Mid_Terrace(row): #17
    return row['f37_Mid_Terrace']\
and not row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f21_Bungalow']\
and not row['f26_Detached_type1']\
and not row['f22_Semi_Detached_type']

def f82_T_Terraced(row): #18
    return row['f41_Terraced']\
and not row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f21_Bungalow']\
and not row['f26_Detached_type1']\
and not row['f27_Detached_type2']\
and not row['f25_Semi_detached']\
and not row['f80_TE_End_Of_Terrace']\
and not row['f81_TM_Mid_Terrace']


def f83_S_Semi_Detached(row): #19
    return row['f25_Semi_detached']\
and not row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f21_Bungalow']\
and not row['f26_Detached_type1']\
and not row['f82_T_Terraced']\
and not row['f80_TE_End_Of_Terrace']\
and not row['f81_TM_Mid_Terrace']


def f84_D_Detached(row): #20
    return row['f30_Detached']\
and not row['f13_Flat']\
and not row['f17_Maisonette']\
and not row['f21_Bungalow']\
and not row['f25_Semi_detached']\
and not row['f41_Terraced']\
and not row['f34_End_Of_Terrace']\
and not row['f37_Mid_Terrace']


def f85_P_Park_Home(row): #21
    return row['f48_Park_Home']

def f86_O_Other(row): #22
    return False

def f87_r1(row): 
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if single_true(l):
        if l[0]:
            return 'Flat semi-detached'        #'FS'
        elif l[1]:
            return 'Flat end-terrace'          #'FE'
        elif l[2]:
            return 'Flat mid-terrace'          #'FM'
        elif l[3]:
            return 'Flat detached'             #'FD'
        elif l[4]:
            return 'Flat'                       #'F'
        elif l[5]:
            return 'Maisonette semi-detached'   #'MS'
        elif l[6]:
            return 'Maisonette end-terrace'     #'ME'
        elif l[7]:
            return 'Maisonette mid-terrace'     #'MM'
        elif l[8]:
            return 'Maisonette detached'         #'MD'
        elif l[9]:
            return 'Maisonette'                  #'M'
        elif l[10]:
            return 'Bungalow semi-detached'      #'BS'
        elif l[11]:
            return 'Bungalow end-of-terrace'     #'BE'
        elif l[12]:
            return 'Bungalow mid-terrace'        #'BM'
        elif l[13]:
            return 'Bungalow detached'           #'BD'
        elif l[14]:
            return 'Bungalow'                    #'B'
        elif l[15]:
            return 'End-of-terrace'              #'TE'         #'End-terrace house'  .  #RM TYPE
        elif l[16]:
            return 'Mid-terrace'                 #'TM'         'Mid-terrace house'      # RM TYPE
        elif l[17]:
            return 'Terraced'                    #'T'        #Terraced house   #RM type
        elif l[18]:
            return 'Semi-detached'               #'S'         #'Semi-detached house' RM TYPE
        elif l[19]:
            return 'Detached'                     #'D'        # 'Detached house'     RM TYPE
        elif l[20]:
            return 'Park Home'                   #'P'        #RM Type
        else: #l[21]:
            return 'Other'                       #'O'
    else:
        return 'NEXT'


    
    
def f88_r2(row):
    PREV = row['f87_r1']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV != 'NEXT'):
        return 'DONE'
    elif l[15] and l[16] and (row['f31_End_Of_Terrace_type'] or row['f38_Terraced_type']):  #if R1 returned NEXT
        if row['f31_End_Of_Terrace_type']:
            return 'End-of-terrace'              #'TE'         #''  .  #RM TYPE
        
        elif row['f38_Terraced_type']:
            return 'Terraced'                    #'T'        #Terraced house   #RM type
    else:
        return 'NEXT'

def f89_r3(row): 
    PREV = row['f88_r2']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    
    if (PREV !='NEXT'):
        return 'DONE'
    elif (row['f88_r2']=='NEXT') and (l[5] or l[6] or l[7]) and row['f14_Maisonette_type']:
        return row['type']#'M'
    else:
        return 'NEXT'

def f90_r4(row): 
    PREV = row['f89_r3']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV!='NEXT'):
        return 'DONE'
    elif(not any(l)) and row['f45_Cottage']:
        return 'Cottage'
    else:
        return 'NEXT'
    
def f91_r5(row): 
    PREV = row['f90_r4']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV !='NEXT'):
        return 'DONE'
    elif (not any(l)) and row['f52_Leasehold'] and row['f64_Retirement_Property']:
        return 'Flat'#'F'
    else:
        return 'NEXT'
    
def f92_r6(row): 
    PREV = row['f91_r5']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV !='NEXT'):
        return 'DONE'
    elif (not any(l)) and row['f55_Freehold'] and row['f64_Retirement_Property']:
        return 'Terraced'                    #'T'        #Terraced house   #RM type
    else:
        return 'NEXT'    
    
def f93_r7(row):
    PREV = row['f92_r6']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV !='NEXT'):
        return 'DONE'    
    elif (not any(l)) and row['f58_Family_Property']:
        return 'Detached'                     #'D'        # 'Detached house'     RM TYPE
    else:
        return 'NEXT'    
    
    
    
def f94_r8(row):
    PREV = row['f93_r7']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV !='NEXT'):
        return 'DONE'
    elif (not any(l)) and row['f55_Freehold'] and row['f64_z_Coach_House_type']:
        return 'Semi-detached'               #'S'         #'Semi-detached house' RM TYPE
    else:
        return 'NEXT'  
    
def f95_r9(row):
    PREV = row['f94_r8']
    l = [
            row['f65_FS_Flat_Semi_Detached'],         #0   FS
            row['f66_FE_Flat_End_Terrace'],           #1   FE
            row['f67_FM_Flat_Mid_Terrace'],           #2   FM
            row['f68_FD_Flat_Detached'],              #3   FD
            row['f69_F_Flat'],                        #4   F
            row['f70_MS_Maisonette_Semi_Detached'],   #5   MS
            row['f71_ME_Maisonette_End_Terrace'],     #6   ME
            row['f72_MM_Maisonette_Mid_Terrace'],     #7   MM
            row['f73_MD_Maisonette_Detached'],        #8   MD
            row['f74_M_Maisonette'],                  #9   M
            row['f75_BS_Bungalow_Semi_Detached'],     #10  BS
            row['f76_BE_Bungalow_End_Terrace'],       #11  BE
            row['f77_BM_Bungalow_Mid_Terrace'],       #12  BM
            row['f78_BD_Bungalow_Detached'],          #13  BD
            row['f79_B_Bungalow'],                    #14  B
            row['f80_TE_End_Of_Terrace'],             #15  TE
            row['f81_TM_Mid_Terrace'],                #16  TM
            row['f82_T_Terraced'],                    #17  T
            row['f83_S_Semi_Detached'],               #18  S
            row['f84_D_Detached'],                    #19  D
            row['f85_P_Park_Home'],                   #20  P
            row['f86_O_Other']                        #21  O
        ]
    if (PREV !='NEXT'):
        return 'DONE'
    elif (not any(l)) and row['f52_Leasehold'] and row['f64_z_Coach_House_type']:
        return 'Flat'#'F'
    else:
        return 'NEXT'  
    
    
def f96_r10(row): 
    PREV = row['f95_r9']
    if (PREV !='NEXT'):
        return 'DONE'
    else:
        return row['type']
    
    
def f97_revised_type(row):
    lis = [
            row['f87_r1'],           #0   FS
            row['f88_r2'],           #1   FE
            row['f89_r3'],           #2   FM
            row['f90_r4'],           #3   FD
            row['f91_r5'],           #4   F
            row['f91_r5'],           #5   MS
            row['f92_r6'],           #6   ME
            row['f93_r7'],           #7   MM
            row['f94_r8'],           #8   MD
            row['f95_r9'],           #9   M
            row['f96_r10']
    ]
    for i in lis:
        if i != 'DONE' and i != 'NEXT':
            return i
        
        
        
revisedType = {
    'PropertyType':['Type4code','Type3code','Type2code','Type1code'],
    'Not Specified':['O','O','O','O'],
    'End-of-Terrace':['TE','T','T','H'],
    'Mid-terrace':['TM','T','T','H'],
    'Flat detached':['FD','F','F','F'],
    'Flat semi-detached':['FS','F','F','F'],
    'Flat end-terrace':['FE','F','F','F'],
    'Flat mid-terrace':['FM','F','F','F'],
    'Maisonette detached':['MD','M','F','F'],
    'Maisonette semi-detached':['MS','M','F','F'],
    'Maisonette end-terrace':['ME','M','F','F'],
    'Maisonette mid-terrace':['MM','M','F','F'],
    'Bungalow end-of-terrace':['BE','B','T','H'],
    'Bungalow mid-terrace':['BM','B','T','H'],
    'Bungalow semi-detached':['BS','B','S','H'],
    'Bungalow detached':['BD','B','D','H'],
    'Cottage':['T','T','T','H'],
    'Chalet':['D','D','D','H'],
    'Character Property':['O','O','O','O'],
    'House of Multiple Occupation':['D','D','D','H'],
    'Terraced':['T','T','T','H'],
    'End of Terrace':['TE','T','T','H'],
    'End-of-terrace':['TE','T','T','H'],
    'Semi-Detached':['S','S','S','H'],
    'Semi-detached':['S','S','S','H'],
    'Detached':['D','D','D','H'],
    'Mews':['T','T','T','H'],
    'Cluster House':['S','S','S','H'],
    'Ground Flat':['F','F','F','F'],
    'Flat':['F','F','F','F'],
    'Studio':['F','F','F','F'],
    'Ground Maisonette':['M','M','F','F'],
    'Maisonette':['M','M','F','F'],
    'Bungalow':['B','B','D','H'],
    'Terraced Bungalow':['B','B','T','H'],
    'Semi-Detached Bungalow':['BS','B','S','H'],
    'Detached Bungalow':['BD','B','D','H'],
    'Mobile Home':['P','P','O','O'],
    'Commercial Property':['O','O','O','O'],
    'Land':['O','O','O','O'],
    'Link Detached House':['D','D','D','H'],
    'Town House':['T','T','T','H'],
    'Cottage Houses':['T','T','T','H'],
    'Chalet Houses':['D','D','D','H'],
    'Character':['O','O','O','O'],
    'House':['T','T','T','H'],
    'Villa':['D','D','D','H'],
    'Apartment':['F','F','F','F'],
    'Penthouse':['F','F','F','F'],
    'Finca':['D','D','D','H'],
    'Barn Conversion':['D','D','D','H'],
    'Serviced Apartments':['F','F','F','F'],
    'Parking':['O','O','O','O'],
    'Sheltered Housing':['O','O','O','O'],
    'Retirement Property':['O','O','O','O'],
    'House Share':['F','F','F','F'],
    'Flat Share':['F','F','F','F'],
    'Park Home':['P','P','O','O'],
    'Garages':['O','O','O','O'],
    'Farm House':['D','D','D','H'],
    'Equestrian Facility':['O','O','O','O'],
    'Duplex':['F','F','F','F'],
    'Triplex':['F','F','F','F'],
    'Longere':['O','O','O','O'],
    'Gite':['O','O','O','O'],
    'Barn':['O','O','O','O'],
    'Trulli':['O','O','O','O'],
    'Mill':['O','O','O','O'],
    'Ruins':['O','O','O','O'],
    'Restaurant':['O','O','O','O'],
    'Cafe':['O','O','O','O'],
    'Mill':['O','O','O','O'],
    'Castle':['O','O','O','O'],
    'Village House':['D','D','D','H'],
    'Cave House':['O','O','O','O'],
    'Cortijo':['D','D','D','H'],
    'Farm Land':['O','O','O','O'],
    'Plot':['O','O','O','O'],
    'Country House':['D','D','D','H'],
    'Stone House':['D','D','D','H'],
    'Caravan':['P','P','O','O'],
    'Lodge':['D','D','D','H'],
    'Log Cabin':['D','D','D','H'],
    'Manor House':['D','D','D','H'],
    'Stately Home':['O','O','O','O'],
    'Off-Plan':['O','O','O','O'],
    'Semi-detached Villa':['S','S','S','H'],
    'Detached Villa':['D','D','D','H'],
    'Bar Nightclub':['O','O','O','O'],
    'Shop':['O','O','O','O'],
    'Riad':['D','D','D','H'],
    'House Boat':['O','O','O','O'],
    'Hotel Room':['O','O','O','O'],
    'Block of Apartments':['O','O','O','O'],
    'Private Halls':['O','O','O','O'],
    'Office':['O','O','O','O'],
    'Business Park':['O','O','O','O'],
    'Serviced Office':['O','O','O','O'],
    'Retail Property (high street)':['O','O','O','O'],
    'Retail Property (out of town)':['O','O','O','O'],
    'Convenience Store':['O','O','O','O'],
    'Garage':['O','O','O','O'],
    'Hairdresser Barber Shop':['O','O','O','O'],
    'Petrol Station':['O','O','O','O'],
    'Post Office':['O','O','O','O'],
    'Pub':['O','O','O','O'],
    'Workshop & Retail space':['O','O','O','O'],
    'Distribution Warehouse':['O','O','O','O'],
    'Factory':['O','O','O','O'],
    'Heavy Industrial':['O','O','O','O'],
    'Industrial Park':['O','O','O','O'],
    'Light Industrial':['O','O','O','O'],
    'Storage':['O','O','O','O'],
    'Showroom':['O','O','O','O'],
    'Warehouse':['O','O','O','O'],
    'Land':['O','O','O','O'],
    'Commercial Development':['O','O','O','O'],
    'Industrial Development':['O','O','O','O'],
    'Residential Development':['O','O','O','O'],
    'Data Centre':['O','O','O','O'],
    'Farm':['O','O','O','O'],
    'Healthcare Facility':['O','O','O','O'],
    'Marine Property':['O','O','O','O'],
    'Mixed Use':['O','O','O','O'],
    'Research & Development Facility':['O','O','O','O'],
    'Science Park':['O','O','O','O'],
    'Guest House':['O','O','O','O'],
    'Hospitality':['O','O','O','O'],
    'Leisure Facility':['O','O','O','O'],
    'Takeaway':['O','O','O','O'],
    'Childcare Facility':['O','O','O','O'],
    'Smallholding':['O','O','O','O'],
    'Place of Worship':['O','O','O','O'],
    'Trade Counter':['O','O','O','O'],
    'Coach House':['F','F','F','F']
        }
            
        
        
def f98_Type4(row):
    key = row['f97_revised_type']
    return revisedType[key][0]

def f99_Type3(row):
    key = row['f97_revised_type']
    return revisedType[key][1]

def f100_Type2(row):
    key = row['f97_revised_type']
    return revisedType[key][2]

def f101_Type1(row):
    key = row['f97_revised_type']
    return revisedType[key][3]
    












