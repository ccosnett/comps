import pandas as pd
import pymysql
import numpy as np
import math
import re
from pandarallel import pandarallel
pandarallel.initialize()
from tqdm.auto import tqdm
tqdm.pandas(desc="attach column")
import time
start_time = time.time()
import pyperclip
pyperclip.copy('The text to be copied to the clipboard.')
spam = pyperclip.paste()



def num_comps_2km_old(aid):
    db = pymysql.connect(
            host='localhost',
            user="alice",
            password='MyStrongPass.',
            port=3306,
            database="avm",
            charset='utf8'
            )
    q1="""SELECT t_lat, t_lon, n_beds, p_type1 FROM district_wirral WHERE aid={aid};""".format(aid=aid)
    out1 = pd.read_sql(q1, db)
    #return out1
    q2="""SELECT SUM(number_of_houses) FROM pc_stats_wirral WHERE MBRContains(LineString(Point({targetLon} + 0.018, {targetLat} + 0.018), Point({targetLon} - 0.018, {targetLat} - 0.018)), coords) AND type1='{targetType1}' AND Beds = {targetNumBedrooms};""".format(targetLon=out1['t_lon'].iloc[0], targetLat=out1['t_lat'].iloc[0], targetType1=out1['p_type1'].iloc[0], targetNumBedrooms=out1['n_beds'].iloc[0])
    out2 = pd.read_sql(q2, db)
    return out2.iloc[0,0]
    
    
def target_vector(aid):
    db = pymysql.connect(
            host='localhost',
            user="alice",
            password='MyStrongPass.',
            port=3306,
            database="avm",
            charset='utf8'
            )
    q="""SELECT * FROM district_wirral WHERE aid = {aid}""".format(aid=aid)
    tic = time.time()
    out = pd.read_sql(q, db)
    toc = time.time()
    db.close()
    print(str(toc-tic) + ' seconds')
    return out


def target_vectors():
    db = pymysql.connect(
            host='localhost',
            user="alice",
            password='MyStrongPass.',
            port=3306,
            database="avm",
            charset='utf8'
            )
    q="""SELECT * FROM district_wirral WHERE pc = 'ch44 4dr'"""
    tic = time.time()
    out = pd.read_sql(q, db)
    toc = time.time()
    db.close()
    print(str(toc-tic) + ' seconds')
    return out




#'aid', 'pc', 'street', 'p_type1', 'p_type2', 'p_type3', 'p_type4',      'pd_type_flag_var', 'tfa', 'n_beds', 't_lat', 't_lon', 'district', 'epcownership', 't_epcrating', 't_tenure', 't_windows',
       #'t_year_construction', 'CF_percent_aggreg_data', 'indexed_target_price', 'nhr', 'num_comps_2km', 'searchRadius', 'searchRadiusKM', 'nearest_postcodes'

def t(string):
    if string == 'NULL':
        return 'NULL'
    else:
        return "'" + string + "'"



def comps(row):
    print('comps')
    db = pymysql.connect(
        host='localhost',
        user="alice",
        password='MyStrongPass.',
        port=3306,
        database="avm",
        charset='utf8'
    )

    street = t(row['street'])
    pc = t(row['pc'])
    epcownership = t(row['epcownership'])
    t_tenure = t(row['t_tenure'])
    t_windows = t(row['t_windows'])
    tfa = row['tfa']
    t_epcrating = t(row['t_epcrating'])
    t_year_construction = t(row['t_year_construction'])
    pd_type_flag_var = row['pd_type_flag_var']
    p_type4 = t(row['p_type4'])
    p_type3 = t(row['p_type3'])
    p_type2 = t(row['p_type2'])
    p_type1 = t(row['p_type1'])
    n_beds = row['n_beds']
    printed_list_of_postcodes = row['printed_list_of_postcodes']
    address_id = row['aid']
    #print(address_id)
    t_lon = row['t_lon']
    t_lat = row['t_lat']
    vars = [street, pc, epcownership, t_tenure, t_windows, tfa, t_epcrating, t_year_construction, pd_type_flag_var, p_type4, p_type3, p_type2, p_type1, n_beds, address_id, t_lat, t_lon]
    print(vars)
    q1 = """SELECT
    *, 1 AS rank
    FROM
    (SELECT
        aid,
								address1line,
								CASE WHEN TOTAL_FLOOR_AREA=0 THEN 0 ELSE 1 END AS has_epc_m2,
								(POWER(POWER(GREATEST( (distance/1609.344),(1/15) ),2), -1) + 50*(CASE WHEN thoroughfare={street} THEN 1 ELSE 0 END) + 50*(CASE WHEN postcode={pc} THEN 1 ELSE 0 END)) AS distance_points,
                                (30*(CASE WHEN EPC_ownership={epcownership} THEN 1 ELSE 0 END)) AS EPC_ownership_points,
                                GREATEST((1-DATEDIFF(CURDATE(),TransactionDate)/3650)*300,0) AS transac_timing_points,
                                IFNULL((SELECT points FROM avm_tenure_att_points WHERE tenure_of_target = {t_tenure} and tenure_of_comp=tenure),0) AS tenure_points,
                                IFNULL((SELECT points FROM avm_window_description_points WHERE target_windows_desc = {t_windows} and comp_windows_desc=windows),0) AS windows_points,
                                CASE WHEN ABS(1-( {tfa} / total_floor_area )) > 0.2 THEN 0 ELSE ((( 0.2 - ABS(1-( {tfa} / total_floor_area ))) / 0.2 ) * 100) END AS M2_points,
								IFNULL((SELECT points FROM avm_EPC_rating_points WHERE target_EPC_rating = {t_epcrating} and comp_EPC_rating=CURRENT_ENERGY_RATING),0) AS EPC_rating_points,
								IFNULL((SELECT points FROM avm_year_construction_points WHERE target_year_construction = {t_year_construction} and comp_year_construction=year_construction),0) AS year_construction_points,
								CASE WHEN {pd_type_flag_var} = 1 THEN CASE WHEN ptm_ah.TYPE4={p_type4} THEN 100 WHEN ptm_ah.TYPE3={p_type3} THEN 80 WHEN ptm_ah.TYPE2={p_type2} THEN 75 WHEN ptm_ah.TYPE1={p_type1} THEN 0 ELSE 0 END ELSE 0 END AS type_points,
							    total_floor_area,
								CASE
								   WHEN ptm_pp.TYPE2='F' THEN
									    final_F_index/init_F_index * p
								   WHEN ptm_pp.TYPE2='D' THEN
									    final_D_index/init_D_index * p
								   WHEN ptm_pp.TYPE2='S' THEN
									    final_S_index/init_S_index * p
								   WHEN ptm_pp.TYPE2='T' THEN
									    final_T_index/init_T_index * p
								   WHEN ptm_pp.TYPE2='O' THEN
									    final_O_index/init_O_index * p
								   END AS indexed_price


							FROM
								(
								SELECT
									ah.aid AS aid,
									ah.address1line,
									SQRT(POWER(({t_lat} - latitude4326)*70.25,2) + POWER(({t_lon} - longitude4326)*42.08,2))*1609.344 AS distance,
									EPC_ownership,
									CURRENT_ENERGY_RATING,
									pp.TransactionDate,
									postcode,
									thoroughfare,
									pp.propertyType AS pp_pType,
									ah.propertyType AS ah_pType,
									CASE WHEN ten.UD IS NOT NULL THEN ten.UD WHEN ten.LR_PP IS NOT NULL THEN ten.LR_PP ELSE AH END AS tenure,
									CASE WHEN window_desc.UD IS NOT NULL THEN window_desc.UD ELSE EPC END AS windows,
									NUMBER_HABITABLE_ROOMS AS nhr,
									TOTAL_FLOOR_AREA AS total_floor_area,
									Indic_year_band_construction AS year_construction,
									lind.latest_F AS final_F_index, ind.F AS init_F_index, lind.latest_D AS final_D_index, ind.D AS init_D_index,
									lind.latest_T AS final_T_index, ind.T AS init_T_index, lind.latest_S AS final_S_index, ind.S AS init_S_index,
									lind.latest_index AS final_O_index, ind.prop_index AS init_O_index,
									ind.price AS p,
									coords
								FROM
									liqquid_addresshub ah
								LEFT JOIN
									liqquid_epc_certs epc ON ah.aid = epc.aid
								LEFT JOIN
									liqquid_pp pp ON ah.aid=pp.aid AND pp.TransactionDate = (SELECT MAX(TransactionDate) FROM liqquid_pp WHERE aid=ah.aid)
								LEFT JOIN
									avm_tenure ten ON ah.aid = ten.aid
								LEFT JOIN
									avm_window_description window_desc ON ah.aid=window_desc.aid
								LEFT JOIN
									avm_year_construction yc ON ah.aid=yc.aid
								LEFT JOIN
									avm_indexed_price ind ON ah.aid = ind.aid
								LEFT JOIN
									avm_latest_index_values lind ON ah.adminDistrictCode = lind.AreaCode
								WHERE postcode IN ({printed_list_of_postcodes})
								) s
							LEFT JOIN
								avm_property_type_matrix ptm_pp ON s.pp_pType = ptm_pp.Type4
							LEFT JOIN
								avm_property_type_matrix ptm_ah ON s.ah_pType = ptm_ah.Type4
							LEFT JOIN
								liqquid_hab_rooms_est lhre ON s.nhr = lhre.Habitable_Rooms
							WHERE
								Beds={n_beds}
							AND
								ptm_ah.TYPE1={p_type1} AND
								p IS NOT NULL AND aid <> {address_id}
							ORDER BY distance
							LIMIT 10) p;""".format(
        street=street,
        pc=pc,
        epcownership=epcownership,
        t_tenure=t_tenure,
        tfa=tfa,
        t_epcrating=t_epcrating,
        t_year_construction=t_year_construction,
        pd_type_flag_var=pd_type_flag_var,
        p_type4=p_type4,
        p_type3=p_type3,
        p_type2=p_type2,

        p_type1=p_type1,
        printed_list_of_postcodes=printed_list_of_postcodes,

        n_beds=n_beds,
        address_id=address_id,
        t_lon=t_lon,
        t_lat=t_lat,
        t_windows=t_windows)
    pyperclip.copy(q1)
    try:
        out = pd.read_sql(q1, db)
       # print('here')
        return 1
    except:
        return 0


